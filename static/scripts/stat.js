function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c === 'x' ? r : (r & 0x3 | 0x8);

        return v.toString(16);
    });
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));

    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires === 'number' && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + '=' + value;

    for (var propName in options) {
        updatedCookie += '; ' + propName;

        var propValue = options[propName];

        if (propValue !== true) {
            updatedCookie += '=' + propValue;
        }
    }

    document.cookie = updatedCookie;
}

var uuid = getCookie('STAT') || guid();

if (!getCookie('STAT')) {
    setCookie('STAT', uuid, {expires: 3600});
}

function send_stat(data) {
    var xhr = new XMLHttpRequest();

    xhr.open('POST', '/stat');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
}

function download_stat(_this) {
    send_stat(JSON.stringify({uuid: uuid, url: _this.href, agent: navigator.userAgent, type: 'download'});
}

var all_download = document.querySelectorAll('a[download]');

for (var i = 0, n = all_download.length; i < n; i++) {
    all_download[i].onclick = download_stat(all_download[i]);
}

send_stat({uuid: uuid, url: window.location.href, agent: navigator.userAgent, type: 'url'});