from datetime import datetime
from flask import Flask, request, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

app = Flask(__name__)
app.config.from_mapping(
    SQLALCHEMY_DATABASE_URI='postgresql://sasha:sasha@localhost:5432/statistics',
    SQLALCHEMY_TRACK_MODIFICATIONS=False)

db = SQLAlchemy(app)
DeclarativeBase = declarative_base()

TYPE_URL = 10
TYPE_DOWNLOAD = 20


class Statistics(db.Model, DeclarativeBase):
    __tablename__ = 'data_statistics'

    id = db.Column(
        UUID(),
        primary_key=True,
        nullable=False,
        index=True,
        unique=True,
        server_default=func.uuid_generate_v4())

    created_on = db.Column(
        db.DateTime(True),
        default=datetime.now(),
        server_default=func.now())

    type = db.Column(
        db.SmallInteger(),
        nullable=True)

    session = db.Column(
        db.String(36),
        nullable=True)

    url = db.Column(
        db.String(255),
        nullable=True)

    agent = db.Column(
        db.Text(),
        nullable=True)

    @property
    def pk(self):
        return self.id


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/page')
def page():
    return render_template('page.html')


@app.route('/stat', methods=('GET', 'POST'), strict_slashes=False)
def stat():
    if request.method == 'POST':
        _url = request.json.get('url', None)
        _type = request.json.get('type', None)

        if _url is not None and _url.endswith('/'):
            _url = _url[:-1]

        if _type is not None:
            _type = dict((('url', TYPE_URL), ('download', TYPE_DOWNLOAD),))[_type]

        row_data = {'session': request.json.get('uuid', None),
                    'url': _url,
                    'agent': request.json.get('agent', None),
                    'type': _type}

        row = Statistics(**row_data)

        db.session.add(row)
        db.session.commit()

        return jsonify({'success': True})

    """ Статистика посещаемости """

    subquery_page_distinct = db.session.query(Statistics.url, Statistics.session).distinct()\
        .filter(Statistics.type == TYPE_URL).subquery('_ds')

    subquery_page = db.session.query(subquery_page_distinct.c.url, func.count(subquery_page_distinct.c.url).label('uni_hits'))\
        .group_by(subquery_page_distinct.c.url).subquery('dss')

    list_page_stats = db.session.query(Statistics.url, subquery_page.c.uni_hits, func.count(Statistics.url).label('hits'))\
        .outerjoin(subquery_page, subquery_page.c.url == Statistics.url) \
        .filter(Statistics.type == TYPE_URL).group_by(Statistics.url, subquery_page.c.uni_hits).all()

    """ Статистика загрузки файлов """

    subquery_download_distinct = db.session.query(Statistics.url, Statistics.session).distinct() \
        .filter(Statistics.type == TYPE_DOWNLOAD).subquery('_ds')

    subquery_download = db.session.query(subquery_download_distinct.c.url, func.count(subquery_download_distinct.c.url).label('uni_hits')) \
        .group_by(subquery_download_distinct.c.url).subquery('dss')

    list_download_stats = db.session.query(Statistics.url, subquery_download.c.uni_hits, func.count(Statistics.url).label('hits'))\
        .outerjoin(subquery_download, subquery_download.c.url == Statistics.url) \
        .filter(Statistics.type == TYPE_DOWNLOAD).group_by(Statistics.url, subquery_download.c.uni_hits).all()

    return render_template('stat.html', list_page_stats=list_page_stats, list_download_stats=list_download_stats)


if __name__ == '__main__':
    app.run()
